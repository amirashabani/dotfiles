#!/bin/bash

# Change the background wallpaper
feh --bg-scale $HOME/.wallpaper/Burning\ Hexagons.png

# Start conky
conky

# Enable persian keyboard layout
setxkbmap -model pc105 -layout us,ir -option grp:alt_shift_toggle

# Enable tapping of touchpad
./pointer.sh
