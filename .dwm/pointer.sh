#!/bin/bash

pointer_id="$(xinput --list | awk '/ALPS GlidePoint/ {print $6}' | tr -d 'id=')"
tapping_enabled="$(xinput --list-props $pointer_id | awk '/Tapping Enabled \(.*\)/ {print $4}' | tr -d '():')"

xinput --set-prop $pointer_id $tapping_enabled 1
