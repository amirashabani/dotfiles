require 'cairo'

function conky_main()
	if conky_window == nil then
		return
	end
	local cs = cairo_xlib_surface_create(
		conky_window.display,
		conky_window.drawable,
		conky_window.visual,
		conky_window.width,
		conky_window.height
	)
	cr = cairo_create(cs)

	-- Font
	FONT_NAME = "SF Pro Display"
	RED, GREEN, BLUE, ALPHA = 1, 1, 1, 1
	FONT_SLANT = CAIRO_FONT_SLANT_NORMAL
	FONT_WIGHT = CAIRO_FONT_WEIGHT_NORMAL

	-- Box
	BOX_DISTANCE_FROM_ABOVE = 50

	-- Element of a Box
	ELEMENT_INITIAL_DISTANCE_FROM_LABEL = 35
	ELEMENT_DISTANCE = 25

	-- The y position of cursor
	CURSOR_Y = 0

	time_box()
	network_box()
	filesystem_box()
	memory_box()
	process_box()
	system_box()

	cairo_destroy(cr)
	cairo_surface_destroy(cs)
	cr = nil
end

function print_to_screen(size, text, xpos, ypos)
	cairo_select_font_face(cr, FONT_NAME, FONT_SLANT, FONT_WEIGHT)
	cairo_set_font_size(cr, size)
	cairo_set_source_rgba(cr, RED, GREEN, BLUE, ALPHA)
	cairo_move_to(cr, xpos, ypos)
	cairo_show_text(cr, text)
	cairo_stroke(cr)
end

function draw_line(line_width, line_cap, startx, starty, endx, endy)
	cairo_set_line_width(cr, line_width)
	cairo_set_line_cap(cr, line_cap)
	cairo_set_source_rgba(cr, RED, GREEN, BLUE, ALPHA)
	cairo_move_to(cr, startx, starty)
	cairo_line_to(cr, endx, endy)
	cairo_stroke(cr)
end

function time_label()
	font_size = 12
	text = "TIME"
	print_to_screen(font_size, text, 50, CURSOR_Y)

	line_width = 1
	line_cap = CAIRO_LINE_CAP_BUTT
	draw_line(line_width, line_cap, 80, CURSOR_Y-5, 375, CURSOR_Y-5) 
end

function unix_time()
	font_size = 45
	text = conky_parse('${time %s}')

	print_to_screen(font_size, text, 50, CURSOR_Y)
end

function normal_time()
	font_size = 20
	text = conky_parse('${time %A - %B %d, %Y}')

	print_to_screen(font_size, text, 50, CURSOR_Y)
end

function time_box()
	-- Print TIME label with a horizontal line
	CURSOR_Y = CURSOR_Y + BOX_DISTANCE_FROM_ABOVE
	time_label()

	-- Print Unix Time
	CURSOR_Y = CURSOR_Y + 45
	unix_time()

	-- Print Normal Time
	CURSOR_Y = CURSOR_Y + 30
	normal_time()
end

function network_label()
	font_size = 12
	text = "NETWORK"

	print_to_screen(font_size, text, 50, CURSOR_Y)

	line_width = 1
	line_cap = CAIRO_LINE_CAP_BUTT
	draw_line(line_width, line_cap, 110, CURSOR_Y-5, 375, CURSOR_Y-5)
end

function network_name()
	font_size = 15
	text = 'Name: ' .. conky_parse('${wireless_essid wlp2s0}')

	print_to_screen(font_size, text, 50, CURSOR_Y)
end

function network_upload()
	font_size = 15
	text = 'Uploaded: ' .. conky_parse('${totalup wlp2s0}')

	print_to_screen(font_size, text, 50, CURSOR_Y)

	text = 'Speed: ' .. conky_parse('${upspeed wlp2s0}')
 	print_to_screen(font_size, text, 270, CURSOR_Y)
end

function network_download()
	font_size = 15
	text = 'Downloaded: ' .. conky_parse('${totaldown wlp2s0}')

	print_to_screen(font_size, text, 50, CURSOR_Y)

	text = 'Speed: ' .. conky_parse('${downspeed wlp2s0}')
	print_to_screen(font_size, text, 270, CURSOR_Y)
end

function network_box()
	-- Print NETWORK label with a horizontal line
	CURSOR_Y = CURSOR_Y + BOX_DISTANCE_FROM_ABOVE
	network_label()

	-- Print Network Name
	CURSOR_Y = CURSOR_Y + ELEMENT_INITIAL_DISTANCE_FROM_LABEL
	network_name()

	-- Print Upload stats
	CURSOR_Y = CURSOR_Y + ELEMENT_DISTANCE
	network_upload()

	-- Print Download stats
	CURSOR_Y = CURSOR_Y + ELEMENT_DISTANCE
	network_download()
end

function memory_label()
	font_size = 12
	text = "MEMORY"

	print_to_screen(font_size, text, 50, CURSOR_Y)

	line_width = 1
	line_cap = CAIRO_LINE_CAP_BUTT
	draw_line(line_width, line_cap, 105, CURSOR_Y-5, 375, CURSOR_Y-5)
end

function memory_usage()
	font_size = 15
	text = 'RAM: ' .. conky_parse('${mem}') .. ' / ' .. conky_parse('${memmax}') .. ' (' .. conky_parse('${memperc}') .. '%)'

	print_to_screen(font_size, text, 50, CURSOR_Y)
end

function memory_box()
	-- Print MEMORY label with a horizontal line
	CURSOR_Y = CURSOR_Y + BOX_DISTANCE_FROM_ABOVE
	memory_label()

	-- Print "memory in use / total amount of memory (percentage of memory in use)"
	CURSOR_Y = CURSOR_Y + ELEMENT_INITIAL_DISTANCE_FROM_LABEL
	memory_usage()
end

function filesystem_label()
	font_size = 12
	text = "FILESYSTEM"

	print_to_screen(font_size, text, 50, CURSOR_Y)

	line_width = 1
	line_cap = CAIRO_LINE_CAP_BUTT
	draw_line(line_width, line_cap, 125, CURSOR_Y-5, 375, CURSOR_Y-5)
end

function filesystem_root()
	font_size = 15
	text = 'Root: ' .. conky_parse('${fs_used /}') .. ' / ' .. conky_parse('${fs_size /}') .. ' (' .. conky_parse('${fs_used_perc /}') .. '%)'

	print_to_screen(font_size, text, 50, CURSOR_Y)
end

function filesystem_boot()
	font_size = 15
	text = 'Boot: ' .. conky_parse('${fs_used /boot}') .. ' / ' .. conky_parse('${fs_size /boot}') .. ' (' .. conky_parse('${fs_used_perc /boot}') .. '%)'

	print_to_screen(font_size, text, 50, CURSOR_Y)
end

function filesystem_home()
	font_size = 15
	text = 'Home: ' .. conky_parse('${fs_used /home}') .. ' / ' .. conky_parse('${fs_size /home}') .. ' (' .. conky_parse('${fs_used_perc /home}') .. '%)'

	print_to_screen(font_size, text, 50, CURSOR_Y)
end

function filesystem_box()
	-- Print FILESYSTEM label with a horizontal line
	CURSOR_Y = CURSOR_Y + BOX_DISTANCE_FROM_ABOVE
	filesystem_label()

	-- Print Root
	CURSOR_Y = CURSOR_Y + ELEMENT_INITIAL_DISTANCE_FROM_LABEL
	filesystem_root()

	-- Print Boot
	CURSOR_Y = CURSOR_Y + ELEMENT_DISTANCE
	filesystem_boot()

	-- Print Home
	CURSOR_Y = CURSOR_Y + ELEMENT_DISTANCE
	filesystem_home()
end

function process_label()
	font_size = 12
	text = "PROCESSES"
	print_to_screen(font_size, text, 50, CURSOR_Y)

	line_width = 1
	line_cap = CAIRO_LINE_CAP_BUTT
	draw_line(line_width, line_cap, 125, CURSOR_Y-5, 375, CURSOR_Y-5)
end

function process_count()
	font_size = 15
	text = 'Total Processes: ' .. conky_parse('${processes}') .. ' (' .. conky_parse('${running_processes}') .. ' running)'

	print_to_screen(font_size, text, 50, CURSOR_Y)
end

function top_processes()
	font_size = 15

	text = 'Top Processes: '
	print_to_screen(font_size, text, 50, CURSOR_Y)

	text = 'PID'
	print_to_screen(font_size, text, 220, CURSOR_Y)

	text = 'CPU'
	print_to_screen(font_size, text, 260, CURSOR_Y)

	text = 'MEM'
	print_to_screen(font_size, text, 300, CURSOR_Y)

	text = 'USER'
	print_to_screen(font_size, text, 340, CURSOR_Y)

	font_size = 10

	CURSOR_Y = CURSOR_Y + 15
	text = conky_parse('${top name 1}')
	print_to_screen(font_size, text, 60, CURSOR_Y)
	text = conky_parse('${top pid 1}')
	print_to_screen(font_size, text, 220, CURSOR_Y)
	text = conky_parse('${top cpu 1}')
	print_to_screen(font_size, text, 260, CURSOR_Y)
	text = conky_parse('${top mem 1}')
	print_to_screen(font_size, text, 300, CURSOR_Y)
	text = conky_parse('${top user 1}')
	print_to_screen(font_size, text, 340, CURSOR_Y)

	CURSOR_Y = CURSOR_Y + 15
	text = conky_parse('${top name 2}')
	print_to_screen(font_size, text, 60, CURSOR_Y)
	text = conky_parse('${top pid 2}')
	print_to_screen(font_size, text, 220, CURSOR_Y)
	text = conky_parse('${top cpu 2}')
	print_to_screen(font_size, text, 260, CURSOR_Y)
	text = conky_parse('${top mem 2}')
	print_to_screen(font_size, text, 300, CURSOR_Y)
	text = conky_parse('${top user 2}')
	print_to_screen(font_size, text, 340, CURSOR_Y)

	CURSOR_Y = CURSOR_Y + 15
	text = conky_parse('${top name 3}')
	print_to_screen(font_size, text, 60, CURSOR_Y)
	text = conky_parse('${top pid 3}')
	print_to_screen(font_size, text, 220, CURSOR_Y)
	text = conky_parse('${top cpu 3}')
	print_to_screen(font_size, text, 260, CURSOR_Y)
	text = conky_parse('${top mem 3}')
	print_to_screen(font_size, text, 300, CURSOR_Y)
	text = conky_parse('${top user 3}')
	print_to_screen(font_size, text, 340, CURSOR_Y)
	
	CURSOR_Y = CURSOR_Y + 15
	text = conky_parse('${top name 4}')
	print_to_screen(font_size, text, 60, CURSOR_Y)
	text = conky_parse('${top pid 4}')
	print_to_screen(font_size, text, 220, CURSOR_Y)
	text = conky_parse('${top cpu 4}')
	print_to_screen(font_size, text, 260, CURSOR_Y)
	text = conky_parse('${top mem 4}')
	print_to_screen(font_size, text, 300, CURSOR_Y)
	text = conky_parse('${top user 4}')
	print_to_screen(font_size, text, 340, CURSOR_Y)

	CURSOR_Y = CURSOR_Y + 15
	text = conky_parse('${top name 5}')
	print_to_screen(font_size, text, 60, CURSOR_Y)
	text = conky_parse('${top pid 5}')
	print_to_screen(font_size, text, 220, CURSOR_Y)
	text = conky_parse('${top cpu 5}')
	print_to_screen(font_size, text, 260, CURSOR_Y)
	text = conky_parse('${top mem 5}')
	print_to_screen(font_size, text, 300, CURSOR_Y)
	text = conky_parse('${top user 5}')
	print_to_screen(font_size, text, 340, CURSOR_Y)
end

function process_box()
	-- Print PROCESSES label with a horizontal line
	CURSOR_Y = CURSOR_Y + BOX_DISTANCE_FROM_ABOVE
	process_label()

	-- Print total and running processes' count
	CURSOR_Y = CURSOR_Y + ELEMENT_INITIAL_DISTANCE_FROM_LABEL
	process_count()

	-- Print Top Processes
	CURSOR_Y = CURSOR_Y + ELEMENT_DISTANCE
	top_processes()
end

function system_label()
	font_size = 12
	text = "SYSTEM"
	print_to_screen(font_size, text, 950, CURSOR_Y)

	line_width = 1
	line_cap = CAIRO_LINE_CAP_BUTT
	draw_line(line_width, line_cap, 1005, CURSOR_Y-5, 1300, CURSOR_Y-5)
end

function operating_system()
	font_size = 15
	text = 'Operating System: ' ..  conky_parse('${execi 3600 uname -o}')
	print_to_screen(font_size, text, 950, CURSOR_Y)
end

function distribution()
	font_size = 15
	text = 'Distribution: ' .. conky_parse('${execi 3600 awk -F\'"\' \'/PRETTY_NAME/{print $2}\' /etc/os-release}')
	print_to_screen(font_size, text, 950, CURSOR_Y)
end

function kernel_version()
	font_size = 15
	text = 'Kernel Version: ' .. conky_parse('${kernel}')
	print_to_screen(font_size, text, 950, CURSOR_Y)
end

function hostname()
	font_size = 15
	text = 'Hostname: ' .. conky_parse('${nodename}')
	print_to_screen(font_size, text, 950, CURSOR_Y)
end

function usernames()
	font_size = 15
	text = 'Logged in users: ' .. conky_parse('${user_names}')
	print_to_screen(font_size, text, 950, CURSOR_Y)
end

function uptime()
	font_size = 15
	text = 'Uptime: ' .. conky_parse('${uptime}')
	print_to_screen(font_size, text, 950, CURSOR_Y)
end

function system_box()
	-- Print SYSTEM label with a horizontal line
	CURSOR_Y = 50
	system_label()
	
	-- Print the name of the operating system
	CURSOR_Y = CURSOR_Y + ELEMENT_INITIAL_DISTANCE_FROM_LABEL
	operating_system()

	-- Print the name of the distribution
	CURSOR_Y = CURSOR_Y + ELEMENT_DISTANCE
	distribution()

	-- Print the version of the Linux Kernel
	CURSOR_Y = CURSOR_Y + ELEMENT_DISTANCE
	kernel_version()

	-- Print hostname
	CURSOR_Y = CURSOR_Y + ELEMENT_DISTANCE
	hostname()

	-- Print the names of the users logged in
	CURSOR_Y = CURSOR_Y + ELEMENT_DISTANCE
	usernames()

	-- Print uptime
	CURSOR_Y = CURSOR_Y + ELEMENT_DISTANCE
	uptime()
end
